#!/usr/bin/perl
use strict;
use warnings FATAL => 'all';
use Net::SNMPTrapd;
use IO::Socket::INET;
use Getopt::Long;

my %options;

GetOptions(\%options, "port|p=i", "localaddr=s", "family=s", "timeout=i");

my $port = $options{'port'} || 162;
my $family = $options{'family'} || AF_INET;
my $timeout = $options{'timeout'} || 10;

my $log = setlog();
my $error_log = $log->{error};
my $output_log = $log->{output};

my $snmp_trapd = Net::SNMPTrapd->new(
    Family    => $family,
    LocalPort => $port,
    Timeout   => $timeout
);

unless (defined($snmp_trapd)) {
    print $error_log "Error creating SNMP trap listener ". Net::SNMPTrapd->error();
    die;
}

# Main loop

while (1) {
    my $trap = $snmp_trapd->get_trap;

    if (!defined($trap)) {
        printf $error_log "$0: %s\n", Net::SNMPTrapd->error;
        exit 1;
    } elsif ($trap == 0) {
        next;
    }

    if (!defined($trap->process_trap())) {
        printf $error_log "$0: %s\n", Net::SNMPTrapd->error;
    } else {
        printf $output_log "%s\t%i\t%i\t%s\n",
            $trap->remoteaddr,
            $trap->remoteport,
            $trap->version,
            $trap->community
    }
    my $p = "";
    for my $vals (@{$trap->varbinds}) {
        for (keys(%{$vals})) {
            $p .= sprintf "%s: %s; ", $_, $vals->{$_}
        }
    }
    print $output_log "$p\n";
}


sub setlog {
    my $o_log = IO::File->new("/tmp/snmp_trap_listener_output".$$, ">");
    my $e_log = IO::File->new("/tmp/snmp_trap_listener_error".$$, ">");
    $o_log->autoflush(1);
    $e_log->autoflush(1);
    open STDOUT, ">&", $o_log;
    STDOUT->autoflush(1);
    open STDERR, ">&", $e_log;
    STDERR->autoflush(1);
    return {output => $o_log, error => $e_log};
}


