# snmp-trap-listener

An utility script written using Net::SNMPTrapd to start SNMP Server to receive traps

## Install

* Download: 
    `curl -O "https://dl.bintray.com/akr-optimus/deb/pool/s/snmp-trap-listener/snmp-trap-listener.deb"`
    or
    [click here](https://bintray.com/akr-optimus/deb/download_file?file_path=pool%2Fs%2Fsnmp-trap-listener%2Fsnmp-trap-listener.deb)
* `dpkg -i snmp-trap-listener.deb`

## Execute

* `sudo perl /usr/bin/snmp_trap_listener.pl [--port=<>] [--localaddr=<>] [--family=<>] [--timeout=<>]`


